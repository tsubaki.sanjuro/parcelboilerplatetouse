import User from './components/User';
import Header from './components/Header';
import Clock from './components/Clock';

import './scss/app.scss';

const app = async () => {
	document.getElementById('header').innerHTML = await Header();
	document.getElementById('user').innerHTML = await User();
	await Clock();
};


// Load app
app();
