const Header = async () => {
	const template = `
    <header>
      <h2>My Parcel App</h2>
			<p>This is a boilerplate for a simple vanilla JS workflow using the Parcel bundler.</p>
			<div id="clock"></div>
    </header>
  `;

	return template;
};

export default Header;
