const moment = require('moment')

const displayTime = async () => {
	let time = moment().format('HH:mm:ss');
	document.getElementById('clock').innerHTML = `
	<div>Time now is : ${time}</div>
	`;
	setTimeout(displayTime, 1000);
}

export default displayTime;